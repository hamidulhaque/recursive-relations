<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Exception;
use GuzzleHttp\Psr7\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    public function store(Request $request){
        // $request->validate([
        //     '' => ['required', 'string', 'max:255'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
        //     'password' => ['required', 'confirmed', Rules\Password::defaults()],
        // ]);
        
     try{
        $data= Menu::create(
            [
                'title' => $request->title,
                'slug' => $request->slug,
                'route_name' => $request->route_name,
                'category_id' => $request->category_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]
        );

        return redirect()->back()->withSuccess('Successfully Created!');
     }catch(Exception $e){
        
      dd($e);
     }
        
    }


    public function view(){
       $menus = Menu::get()->all();
    //    dd($menus);
       return view('newdashboard',compact('menus')); 
    }

   

}
