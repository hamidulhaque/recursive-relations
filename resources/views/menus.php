<x-backend.layouts.master>

    {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Holy guacamole!</strong> You should check in on some of those fields below.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"></button>
    </div>


    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <li></li>
    </div> --}}
    @if (session('message'))
        <p class="alert alert-success">{{ session('message') }}</p>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card-body">
                <form action="{{ route('category.store') }}" method="POST">
                    @csrf
                    @method('post')
                    <fieldset class="mb-3">

                        <legend class="text-uppercase font-size-sm font-weight-bold">Add Categories</legend>


                        <div class="form-group row mb-3">
                            <label class="col-form-label col-lg-2">Category Name</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                            </div>
                        </div>


                        <div class="form-group row mb-3">
                            <label class="col-form-label col-lg-2">Route Name</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="route_name"
                                    value="{{ old('route_name') }}">
                            </div>
                        </div>


                        <div class="form-group row mb-3">
                            <label class="col-form-label col-lg-2">Slug</label>
                            <div class="col-lg-10">
                                <input type="text" name="slug" class="form-control" value="{{ old('slug') }}">
                            </div>
                        </div>


                        <div class="form-group row mb-3">
                            <label class="col-form-label col-lg-2">Under Category</label>
                            <div class="col-lg-10">
                                <select class="form-control" name="category_id">

                                    <option value="" selected>None</option>
                                    @foreach ($menus as $menu)
                                        <option value="{{ $menu->id }}">{{ $menu->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                        </div>

                </form>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Categories</h6>
                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Title</th>
                                    <th>Route</th>
                                    <th>Slug</th>
                                    <th>Parent Category Id</th>
                                    <th>Has Child</th>
                                    <th>Created_by</th>
                                    <th>Updated_by</th>
                                    <th>At</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if ($menus)


                                    @foreach ($menus as $menu)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $menu->title }}</td>
                                            <td>{{ $menu->route_name }}</td>
                                            <td>{{ $menu->slug }}</td>
                                            <td>{{ $menu->parent->title ?? 'Parent' }}</td>
                                            <td>{{ $menu->child->count() ?? '' }}</td>
                                            <td>{{ $menu->createdBy->name ?? 'error' }}</td>
                                            <td>{{ $menu->updatedBy->name ?? null }}</td>
                                            <td>{{ \Carbon\Carbon::parse( $menu->created_at)->diffForHumans() ?? 'error' }}</td>
                                        </tr>
                                    @endforeach

                                @endif


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-backend.layouts.master>
